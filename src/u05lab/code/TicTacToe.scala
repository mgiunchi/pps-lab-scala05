package u05lab.code

object TicTacToe extends App {

  //Un giocatore ha il metodo other che restituisce X oppure O
  sealed trait Player{
    def other: Player = this match {
    case X => O;
    case _ => X
  }

  override def toString: String = this match {
    case X => "X";
    case _ => "O"}
  }

  case object X extends Player
  case object O extends Player

  case class Mark(x: Double, y: Double, player: Player)
  type Board = List[Mark] //una board contiene una lista di x,y e player(O,X)
  type Game = List[Board] //un Game contiene una lista di Board

  //deve restituire se data una Board è presente una X o una O in corrispondenza di x o y
  def find(board: Board, x: Double, y: Double): Option[Player] = {
    var myPlayer: Option[Player] = None
    board.foreach(mark=> { if (mark.x==x && mark.y==y) myPlayer=Option(mark.player)})
    myPlayer
  }
//    board.find(mark =>mark.x==x  && mark.y==y).map(mark => mark.player)

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    var myGame: Seq[Board] = Seq[Board]() //creo una nuova lista [board]
    for (x <- 0 to 2; y <- 0 to 2) {
        if (!board.exists(mark => mark.x == x && mark.y == y)) { //se la posiz non contiene X o O, altrime salto
          //per ogni ciclo creo un game: questo game ha solo una X in posiz x,y
          //tutte le altre posiz sono empty (la funzione string inserisce un ".")
          myGame = myGame :+ ((Mark(x, y, player))::board) //la X in posiz x,y la pongo in testa a board
        }
    }
    myGame
  }

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = ???

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse (".")) //se c'è X o O in corrips di x,y, stampa, altrim "."
      if (x == 2) { print(" "); //se ho completato la prima tripletta, metto uno spazio
        if (board == game.head) println()} //se sono giunto alla testa del game vado a capo
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 4) foreach {g => printBoards(g); println()}
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
}